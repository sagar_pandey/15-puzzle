(function ()
{	
	var state = 1;
    var puzzle = document.getElementById('puzzle');

    document.getElementById('solve').addEventListener('click', solve);
    document.getElementById('shuffle').addEventListener('click', shuffle);

    // Getter for  cells
    function getCell(row, col) {
        return document.getElementById('cell-' + row + '-' + col);
    }

    //Getter for an empty cell
    function getEmptyCell() {
        return puzzle.querySelector('.empty'); // Returns the "empty" element.			
    }
	
	
	// Listens for click on puzzle cells
    puzzle.addEventListener('click', function (event)
    {
        if (state == 1)
        {
			// Enabling the sliding animation
			puzzle.className = 'slide';
			shiftCell(event.target);
		}
    });

    solve();
    
	//Creating the solved puzzle automatic 
    function solve()
    {
        if (state == 0)
        {
            return;
        }

        puzzle.innerHTML = '';

		var n = 1;
        for (var i = 0; i <= 3; i++) // rows
        {
            for (var j = 0; j <= 3; j++) // cols
            {
                var cell = document.createElement('div');//creates a new span element
                cell.id = 'cell-' + i + '-' + j; // div id = 'cell-...-...'
				cell.style.left = ((j * 50) + (1 * j) + 1) + 'px'; // width of the cell is 50px and padding-left to 1px.
                cell.style.top = ((i * 50) + (1 * i) + 1) + 'px'; //  height of the cell is 50px and padding-top to 1px.
				
                if (n <= 15)
                {
                    cell.className = 'number'; 
                    cell.innerHTML = n++; //displaying the number in each cell
                }
                else
                {
					cell.className = 'empty';
				}
				
                puzzle.appendChild(cell); // adds a numbered cell after puzzle div and then keeps adding on until the rows and cols are filled up.
			}
        }
        
    }

	//shuffling the puzzle
    function shuffle() {
        state = 0;

        var previousCell;
        var i = 1;
        var interval = setInterval(function () {
            if (i <= 20) {
                var adjacentCell = getAdjacentCells(getEmptyCell());

                if (previousCell) {
                    for (var j = adjacentCell.length - 1; j >= 0; j--) {
                        if (adjacentCell[j].innerHTML == previousCell.innerHTML) {
                            adjacentCell.splice(j, 1);
                        }
                    }
                }

                previousCell = adjacentCell[random(0, adjacentCell.length - 1)];
                shiftCell(previousCell);
                i++;
            }
            else {
                clearInterval(interval);
                state = 1;
            }

        }, 2);
    }

    // Shifting the number cell to the empty cell
    function shiftCell(cell)
    {
        // Checks for number in selected cell
        if (cell.className != 'empty')
        {
            // Tries to get an empty adjacent cell
            var emptyCell = getEmptyAdjacentCell(cell);

            if (emptyCell)
            {
                // Temporary data
                var temp = { style: cell.style.cssText, id: cell.id }; // temp is assigned the css style and cell id.

                // Exchanges id and style values
                cell.style.cssText = emptyCell.style.cssText;
                cell.id = emptyCell.id;
                emptyCell.style.cssText = temp.style;
                emptyCell.id = temp.id;

                if (state == 1)
                {
                    // Checking the order of numbers
                    checkNumberOrder();
                }
            }
        }
    }

    //Checks whether the order of numbers is correct
    function checkNumberOrder()
    {
        // Checks if the empty cell is in correct position i.e. the last cell.
        if (getCell(3, 3).className != 'empty') 
        {
            return;
        }

        var n = 1;
        // Checking num , going through all cells
        for (var i = 0; i <= 3; i++) {
            for (var j = 0; j <= 3; j++) {
                if (n <= 15 && getCell(i, j).innerHTML != n.toString()) {
                    // if the puzzle order is incorrect
                    return;
                }
                n++;
            }
        }
        
        alert("Congratulations!! You've solved the puzzle!!");
    }
    
    //Gets all adjacent cells
    function getAdjacentCells(cell)
    {		
		var id = cell.id.split('-'); // id = cell-0-1. so after spliting, id becomes "cell","0","1" i.e. an array with 3 elements.
		
		// Gets cell position indexes
		var row = parseInt(id[1]); // as array starts from 0 row is in the array[1].
		var col = parseInt(id[2]); // and col is in the array[2].
		
		var adjacentCell = [];
		
		// Gets all possible adjacent cells
		if (row < 3){ adjacentCell.push(getCell(row + 1, col)); } // checks the adjacent bottom cell.		
		if (row > 0){ adjacentCell.push(getCell(row - 1, col)); } // checks the adjacent top cell.
        if (col < 3) { adjacentCell.push(getCell(row, col + 1)); } // checks the adjacent right cell.
        if (col > 0) { adjacentCell.push(getCell(row, col - 1)); } // checks the adjacent left cell.
		
		return adjacentCell;	// length is either 2, 3 or 4 depending on the position of the cell, and one of them will be empty.	
    }
   
    function random(from, to)
    {
		return Math.floor(Math.random() * (to - from + 1)) + from;
    }

	//Gets empty adjacent cell if it exists
    function getEmptyAdjacentCell(cell)
    {		
		// Getting all adjacent cells
		var adjacentCell = getAdjacentCells(cell);
		
		// Searches for empty cell
        for (var i = 0; i < adjacentCell.length; i++)
        {
            if (adjacentCell[i].className == 'empty')
            {
				return adjacentCell[i];
			}
		}		
		
		return false;		
	}

	
    
    

}());